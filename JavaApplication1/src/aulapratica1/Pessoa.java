/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aulapratica1;

/**
 *
 * @author 60003092
 */
public class Pessoa {
    int codigo; 
    String nome; 
    
    public Pessoa (int codigo, String nome) {
        this.codigo=codigo;
        this.nome=nome; 
    }
    
    public String getNome(){
        return nome;        
    }
    
    public void setNome (String nome ){
        this.nome=nome;
    }
    
    
      public int getCodigo(){
        return codigo;        
    }
    
    public void setCodigo (int codigo ){
        this.codigo=codigo;
    }
    
}
