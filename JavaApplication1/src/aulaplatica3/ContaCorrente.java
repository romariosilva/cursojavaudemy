/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aulaplatica3;

/**
 *
 * @author 60003092
 */
public class ContaCorrente {
    private int numero , agencia ; 
    private float limite ; 
    
    ContaCorrente (int numero , int agencia){
        
        this.numero=numero; 
        this.agencia=agencia; 
    }
    
     ContaCorrente (int numero , int agencia , float limite){
        this.numero=numero; 
        this.agencia=agencia; 
        this.limite=limite; 
     }
    
     public int  getNumero(){
        return numero; 
         
     }
      public int  getAgencia(){
        return agencia;          
     }
      
      public float  getLimite(){
        return limite;          
     }
      
    public void setNumero (int numero){
        this.numero=numero; 
    }
     
    public void setAgencia (int agencia){
        this.agencia=agencia;   
    }
    
    public void setLimite (int limite){
        this.limite=limite;   
    }
    
}
